# Rizk MEW LICKD demo scene

Notes:
1. This scene has 30 parcels, and, as of Feb 14, 2023, all parcels as well as the base parcel are listed as supported by LICKD.
2. The src\game.ts file has several sections of source code, marked Test 1, Test 2, Test 3, etc.
Each issue below contains steps to reproduce, naming which ONE of those sections need to be un-commented to do the test.
3. There are older setups and tests for some earlier versions of LICKD in branches of this repo named for the LICKD version number.


## Using v0.12.0
Setup to do before doing steps to reproduce in the issues below
1. Pull the the scene from the master branch of the repo
2. `rmdir /s node_modules` / y
3. `npm i`
4. `dcl build`


### Issue 1: in SDK preview, sound doesn't start by just moving the avatar, whether using zones or not.
if you do a dcl-start, and then when the avatar renders in the scene, tha avatar the sound doesn't start, even if you move the avatar.

You will notice that F12 Debug Console shows, early in the log:
`The AudioContext was not allowed to start. It must be resumed (or created) after a user gesture on the page.`

Steps to reproduce:
1. Enable only the Test 1 section of the src/game.ts file, and save the file (also occurs with Test 2 section)
2. `dcl start`
3. When the avatar and scene render, move the avatar
// expected: audio
// actual no audio

Workarounds
a) Run out of the scene and back in
b) When you do `dcl start`, then as soone as the loading tab appears in the browser, immediately click on the browswer window, before the warning.  Open F12 Debug Console.  If you see the warning, you waited too long to click.  Note that workaround b doesn't require moving the avatar to hear the audio.


### Issue 2:  Without a Zone, you get intermittent audio dropouts when you run between valid parcel boundaries in the scene
Steps to reproduce:
1. Edit the src/game.ts as needed to enable only the Test1 secion (new Player with no zone)

Steps to reproduce:
1. Enable the Test 1 (no zone, no schedule) section of src\game.ts, and save the file
2. `dcl start`
3. Use workaround a) or b) shown in Issue 2 to get sound
4. Turn on F12 Debug Console
5.  Once you hear sound, run around in the scene, watching the console.  Best is to zig/zag across one of the parcel boundar lines.
// expected: continuous sound
// actual: sound often drops upon crossing a parcel boundary, consistent with errors in the conole, and the sounds might not readily restart unless you run out of the scene and back in, or cross a boundary that happens to successfully restart the sound.  
The error reported in console is POST https://chorus.lickd.com/api/listener/signout/dcl 501


### Issue 3 - visible zone box  *** VISIBILITY RESOLVED - User error:  turn debug off or remova that attribute from PlayerConfig
If you use the zone property of PlayerConfig, a visible bounding box is created that matches the "zone" property of the PlayerConfig
It is transparent looking outward, but if your camera is outside the box, you can see it as an opaque, no-materials, box with no collider.

Test 2 uses a zone that fills the scene x,y and is y = 5 M, you can see the outside of the bounding zone
To see the bounding zone, swing your camera up to look down on the avtar, or go out of the scene and look back, or jump (space bar)
This demo scene uses a bounding box that has scale.y = 5, so about 2.5 meters above ground, so over Avatar's head

Steps to reproduce:
1. Enable only the Test 2 (zone, no schedule) section of src\game.ts, and save the file
2. `dcl start`
3. Use one of the workarounds above to hear sound if you wish
// expected: you don't see any zone bounding box appear
// actual: you will see the box if you exit the scene and look back, or if you jump, or if, in 3rd person view, you move your camera to look down on avatar fro above.

Note: the LICKD code seems to use the zone.scale.y as the distance above the ground in which to enable the zone, which is twice as high as the top of the box.  This probably means that the box entity itself isn't being used and could be eliminated.

Workaround:
* To hide the box, set PlayerConfig debug to false
* in 0.11.8 and 0.12.0, to get the audio zone as high as you want, set the zone.scale.y to the height you want,as if the position of the audio zone was from the ground up, (even though the debug:true visible box is centered at postion.y)

### Issue 4 - schedules don't work as expected *** RESOLVED - User error: months in UTC start at Jan = 0


