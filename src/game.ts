import * as chorus from '@lickd/chorus-dcl'

/////// Test 1 -- no zone or schedule
// let lickd = new chorus.Player('/sample/stream.ogg')


////// Test 2 -- zone, but no schedule
const scale: Vector3 = new Vector3(96, 5, 80)
const position: Vector3 = new Vector3(0, 0, -24)

// For testing until Fri Feb 17 at 12:00 UTC
new chorus.Player('/sample/stream.ogg', {
  zones: [new Transform({scale, position})],
  debug: false
})


// Test 3 - zone and schedule
// const scale: Vector3 = new Vector3(96, 5, 80)
// const position: Vector3 = new Vector3(0, 0, -24)

// // Friday, Feb 17 from 12:00am to 07:00am (UTC)
// new chorus.Player('/sample/stream.ogg', {
//   zones: [new Transform({scale, position})],
//   schedule: {
//     start: new Date(Date.UTC(2023, 2, 17, 0, 0, 0)),
//     end: new Date(Date.UTC(2023, 2, 17, 7, 0, 0))
//   }
// })

// // Saturday, Feb 18 from 12:00am to 07:00am (UTC)
// new chorus.Player('/sample/stream.ogg', {
//   zones: [new Transform({scale, position})],
//   schedule: {
//     start: new Date(Date.UTC(2023, 2, 18, 0, 0, 0)),
//     end: new Date(Date.UTC(2023, 2, 18, 7, 0, 0)),
//   }
// })
